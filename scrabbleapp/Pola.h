
/**
Enum reprezentujący rodzaj premii za konkrente pole
*/
typedef enum Rodzaj
{
		SLOWNA, LITEROWA, BRAK
} Rodzaj;


/**
Klasa abstrakcyjna reprezentująca każde pole na planszy wraz z przelicznikiem punktowym
za dane pole i rodzajem ewentualnej premii.
*/
class Pole
{
	bool czyZapisane;
	char litera;
public:
	char getLitera()
	{
		return litera;
	}
	void setLitera(char literaN)
	{
		litera = literaN;
	}

	//tylko do testow
	Pole()
	{
		czyZapisane = false;
		litera = '*';
	}
	virtual int getPunkty()
	{
		return 1;
	}
	virtual Rodzaj getRodzajPremii()
	{
		return BRAK;
	}
	virtual void setCzyZapisane(bool czy)
	{
		czyZapisane = czy;
	}
	virtual bool getCzyZapisane()
	{
		return czyZapisane;
	}
	virtual ~Pole(){};
};

/**
Klasa reprezentująca zwykłe pole na planszy, tzn pole bez dodatkowych premii
*/
class ZwyklePole: public Pole
{
	bool czyZapisane;
	int punkty;
	char litera;
public:
	char getLitera()
	{
		return litera;
	}
	void setLitera(char literaN)
	{
		litera = literaN;
	}
	ZwyklePole(int premiaPunktowa)
	{
		punkty =  premiaPunktowa;
		litera = '*';
		czyZapisane = false;
	}
	virtual int getPunkty()
	{
		return 1;
	}
	virtual Rodzaj getRodzajPremii()
	{
		return BRAK;
	}
	virtual void setCzyZapisane(bool czy)
	{
		czyZapisane = czy;
	}
	virtual bool getCzyZapisane()
	{
		return czyZapisane;
	}
};

class PoleLiterowe: public Pole
{
	bool czyZapisane;
	int punkty;
	char litera;
public:
	char getLitera()
	{
		return litera;
	}
	void setLitera(char literaN)
	{
		litera = literaN;
	}
	PoleLiterowe(int premiaPunktowa)
	{
		czyZapisane = false;
		punkty = premiaPunktowa;
		litera = '*';
	}
	virtual int getPunkty()
	{
		return punkty;
	}
	virtual Rodzaj getRodzajPremii()
	{
		return LITEROWA;
	}
		virtual void setCzyZapisane(bool czy)
	{
		czyZapisane = czy;
	}
	virtual bool getCzyZapisane()
	{
		return czyZapisane;
	}
};


class PoleSlowne: public Pole
{
	bool czyZapisane;
	int punkty;
	char litera;
public:
	char getLitera()
	{
		return litera;
	}
	void setLitera(char literaN)
	{
		litera = literaN;
	}
	PoleSlowne(int premiaPunktowa)
	{
		litera = '*';
		czyZapisane = false;
		punkty = premiaPunktowa;
	}
	virtual int getPunkty()
	{
		return punkty;
	}
	virtual Rodzaj getRodzajPremii()
	{
		return LITEROWA;
	}
		virtual void setCzyZapisane(bool czy)
	{
		czyZapisane = czy;
	}
	virtual bool getCzyZapisane()
	{
		return czyZapisane;
	}
};
