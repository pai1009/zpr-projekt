#include "Pola.h"
#include <boost/python.hpp>

/**
Klasa pisana wg wzorca singletone, reprezentująca Plansze gry wraz z polami.
*/
class Plansza {
    Pole * plansza[15][15];

public:
    Plansza();
    ~Plansza();

    /**
    Funkcja zwracajaca mnoznik punktowy za konkretne pole
    */
    int getPunktyZaPole(int x, int y);
};

using namespace boost::python;

BOOST_PYTHON_MODULE(plansza)
{
    class_<Plansza>("Plansza", init<>())
	.def("getPunktyZaPole", &Plansza::getPunktyZaPole);
}
