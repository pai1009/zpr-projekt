# coding=utf-8
from collections import deque
import random
import datetime

from django.contrib.auth.models import User
from django.db import models
import picklefield
from django_enumfield import enum


class Board(models.Model):
    board_template = [['' for _ in range(15)] for _ in range(15)]
    board = picklefield.PickledObjectField(default=board_template)

    def put_word(self, letters):
        """
        Wstaw slowo do planszy. Instancja gry
        @param letters: slownik z polami: letter, x, y
        """
        for letter in letters:
            x = letter['x']
            y = letter['y']
            l = letter['letter']

            self.board[y][x] = l

    def can_be_added(self, letters):
        if len(letters) == 0:
            return True

        if len(letters) == 1:
            if not self.board[letters[0]['y']][letters[0]['x']]:
                return True
            else:
                return False

        x_changed = False
        y_changed = False

        for i in xrange(1, len(letters)):
            if letters[i]['x'] != letters[i - 1]['x']:
                x_changed = True

            if letters[i]['y'] != letters[i - 1]['y']:
                y_changed = True

        if y_changed and x_changed:
            return False

        if letters[0]['x'] == letters[1]['x']:
            ys = {letter['y'] for letter in letters}

            x = letters[0]['x']
            min_y = min(ys)
            max_y = max(ys)

            for y in ys:
                if self.board[y][x]:
                    return False

            left_out_ys = {i for i in range(min_y, max_y + 1)} - ys

            for y in left_out_ys:
                if not self.board[y][x]:
                    return False

        elif letters[0]['y'] == letters[1]['y']:
            xs = {letter['x'] for letter in letters}

            y = letters[0]['y']
            min_x = min(xs)
            max_x = max(xs)

            for x in xs:
                if self.board[y][x]:
                    return False

            left_out_xs = {i for i in range(min_x, max_x + 1)} - xs

            for x in left_out_xs:
                if not self.board[y][x]:
                    return False

        return True


class Player(models.Model):
    user = models.OneToOneField(User)

    def __unicode__(self):
        return self.user.username


class Game(models.Model):
    _distribution = {'E': (12, 1), 'A': (9, 1), 'I': (9, 1), 'O': (8, 1), 'N': (6, 1),
                     'R': (6, 1), 'T': (6, 1), 'L': (4, 1), 'S': (4, 1), 'U': (4, 1),
                     'D': (4, 2), 'G': (3, 2),
                     'B': (2, 3), 'C': (2, 3), 'M': (2, 3), 'P': (2, 3),
                     'F': (2, 4), 'H': (2, 4), 'V': (2, 4), 'W': (2, 4), 'Y': (2, 4),
                     'K': (1, 5),
                     'J': (1, 8), 'X': (1, 8),
                     'Q': (1, 10), 'Z': (1, 10)}

    class GameState(enum.Enum):
        WAITING_FOR_PLAYERS = 0
        READY_TO_START = 1
        RUNNING = 2
        ENDED = 3

    players = models.ManyToManyField(Player, related_name='games')
    players_ids_queue = picklefield.PickledObjectField(default=deque())
    active_player = models.ForeignKey(Player, related_name='active_games', null=True)

    date = models.DateField(default=datetime.datetime.now)
    letters = picklefield.PickledObjectField()
    board = models.OneToOneField(Board)
    state = enum.EnumField(GameState, default=GameState.WAITING_FOR_PLAYERS)

    @classmethod
    def create(cls):
        g = Game()
        g.letters = list(''.join(c * freq for c, (freq, points) in Game._distribution.iteritems()))
        random.shuffle(g.letters)
        return g

    def add_player(self, player):
        if self.state == Game.GameState.WAITING_FOR_PLAYERS and player not in self.players.all() \
                and len(self.players.all()) < 4:
            self.players.add(player)
            self.players_ids_queue.append(player.id)

            if len(self.players.all()) == 2:
                self.state = Game.GameState.READY_TO_START

            return True
        else:
            return False

    def start_game(self):
        if self.state == Game.GameState.READY_TO_START:
            self.state = Game.GameState.RUNNING
            self.players_ids_queue.rotate(-1)
            self.active_player = self.players.get(id=self.players_ids_queue[-1])
            return True
        else:
            return False

    def end_game(self):
        if self.state == Game.GameState.RUNNING:
            self.state = Game.GameState.ENDED

    def next_player(self):
        if self.state == Game.GameState.RUNNING:
            self.players_ids_queue.rotate(-1)
            self.active_player = self.players.get(id=self.players_ids_queue[-1])

    def pop_tiles(self, n):
        if self.state != Game.GameState.ENDED:
            n = min(len(self.letters), n)
            i = len(self.letters) - n
            tiles = self.letters[i:]
            self.letters = self.letters[:i]

            if i == 0:
                self.end_game()

            self.save()
            return tiles
        else:
            return []

    def remove_player(self, player):
        if self.state != Game.GameState.ENDED and player in self.players.all():
            self.players.remove(player)
            self.players_ids_queue.remove(player.id)


class Score(models.Model):
    player = models.ForeignKey(Player)
    game = models.ForeignKey(Game)
    score = models.IntegerField()


class InGameMetadata(models.Model):
    player = models.ForeignKey(Player)
    game = models.ForeignKey(Game)
    tiles = picklefield.PickledObjectField()
    last_time_refreshed = models.BooleanField(default=True)
    points = models.IntegerField(default=0)

