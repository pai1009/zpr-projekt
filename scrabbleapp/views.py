# coding=utf-8
import json

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.http.response import Http404, HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext

from scrabbleapp.forms import UserForm
from scrabbleapp.models import Game, Player, Board, InGameMetadata

# noinspection PyUnresolvedReferences
from dictionary import Dictionary

# noinspection PyUnresolvedReferences
from plansza import Plansza


dictionary = Dictionary("scrabbleapp/slownik.txt")
plansza = Plansza()


def main(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/profile')
    else:
        return HttpResponseRedirect('/login')


def register(request):
    context = RequestContext(request)
    registered = False

    if request.method == 'POST':
        user_form = UserForm(data=request.POST)

        if user_form.is_valid():
            user = user_form.save()
            assert isinstance(user, User)

            user.set_password(user.password)
            user.save()

            player = Player()
            player.user = user
            player.save()

            registered = True
        else:
            print user_form.errors
    else:
        user_form = UserForm()

    return render_to_response('register.html', {'user_form': user_form, 'registered': registered}, context)


def user_login(request):
    context = RequestContext(request)
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('scrabbleapp.views.profile'))

    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(username=username, password=password)
        """:type: User"""

        if user:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect('/profile')
            else:
                return HttpResponse('Konto nieaktywne.')
        else:
            return HttpResponse('Nie ma takiego uzytkownika.')
    else:
        return render_to_response('login.html', {}, context)


@login_required(login_url='/login')
def user_logout(request):
    logout(request)
    return HttpResponseRedirect('/login')


@login_required(login_url='/login')
def profile(request, errors=''):
    username = request.user.username
    games = Game.objects.all()
    player = Player.objects.get(user=request.user)
    return render_to_response('profile.html',
                              {'username': username, 'games': games, 'player': player, 'errors': errors})


@login_required(login_url='/login')
def create_game(request):
    board = Board()
    board.save()

    game = Game.create()
    game.board = board
    game.save()

    return HttpResponseRedirect(reverse('scrabbleapp.views.profile', args=()))


@login_required(login_url='/login')
def start_game(request, game_id):
    game = Game.objects.get(id=game_id)
    game.start_game()
    game.save()

    return HttpResponseRedirect(reverse('scrabbleapp.views.play_game', args=(game_id,)))


@login_required(login_url='/login')
def play_game(request, game_id):
    game = Game.objects.get(id=game_id)
    player = game.players.get(user=request.user)
    players = game.players.all()
    metadata = InGameMetadata.objects.get(player=player, game=game)

    if game.state == Game.GameState.WAITING_FOR_PLAYERS:
        state = 'waiting'
    elif game.state == game.GameState.READY_TO_START:
        state = 'ready'
    elif game.state == game.GameState.RUNNING:
        state = 'running'
    elif game.state == game.GameState.ENDED:
        state = 'ended'
    else:
        state = 'unknown'

    return render_to_response('board.html', {
        'tiles': metadata.tiles, 'players': players, 'game_id': game_id, 'board': game.board.board, 'state': state,
        'players_turn': game.active_player == player, 'points': metadata.points, 'tiles_left': len(game.letters),
        'players_name': player.user.username
    })


@login_required(login_url='/login')
def add_player_to_game(request, game_id):
    player = Player.objects.get(user=request.user)
    game = Game.objects.get(id=game_id)

    if game.add_player(player):
        game.save()

        metadata = InGameMetadata(game=game, player=player)
        metadata.tiles = game.pop_tiles(7)
        metadata.save()

        return HttpResponseRedirect(reverse('scrabbleapp.views.play_game', args=(game_id,)))
    else:
        return profile(request, errors='Nie udało się dodać gracza.')


@login_required(login_url='/login')
def remove_player_from_game(request, game_id):
    player = Player.objects.get(user=request.user)
    game = Game.objects.get(id=game_id)
    game.remove_player(player)
    game.save()

    InGameMetadata.objects.filter(player=player, game=game).delete()
    return HttpResponseRedirect('/profile')


@login_required(login_url='/login')
def add_word(request, game_id):
    if request.method == 'GET' and request.is_ajax():
        game = Game.objects.get(id=game_id)
        active_player = game.active_player
        player = Player.objects.get(user=request.user)

        if game.state == Game.GameState.RUNNING and player == active_player:

            letters = request.GET.get('letters', json.dumps([]))
            letters = json.loads(letters)

            letters_num = len(letters)

            if not game.board.can_be_added(letters):
                return HttpResponse(json.dumps(False))

            game.board.put_word(letters)
            game.board.save()

            tiles = game.pop_tiles(letters_num)

            game.next_player()
            game.save()

            metadata = InGameMetadata.objects.get(game=game, player=player)

            for letter in letters:
                metadata.tiles.remove(letter['letter'])
                metadata.points += plansza.getPunktyZaPole(letter['x'], letter['y'])

            metadata.tiles += tiles

            metadata.save()
            # res = dictionary.exists(word.encode('utf-8'))
            # return render_to_response('board.html', {'tiles': tiles, 'game_id': game_id})
        else:
            return HttpResponse(json.dumps(False))

        return HttpResponse(json.dumps(True))
    else:
        raise Http404()


@login_required(login_url='/login')
def needs_reloading(request, game_id):
    if request.method == 'GET' and request.is_ajax():
        player = Player.objects.get(user=request.user)
        game = Game.objects.get(id=game_id)

        waiting_for_players = game.state == Game.GameState.WAITING_FOR_PLAYERS
        ready_to_start = game.state == Game.GameState.READY_TO_START
        current_player = game.state == game.GameState.RUNNING and not game.active_player == player

        condition = waiting_for_players or ready_to_start or current_player
        condition_copy = condition

        metadata = InGameMetadata.objects.get(player=player, game=game)

        # jezeli ostatnim razem bylo odswiezone, a teraz juz nie, to warto jeszcze raz odswiezyc
        # w takim wypadku bedzie odswiezenie po zmianie stanu
        if metadata.last_time_refreshed:
            condition = True

        metadata.last_time_refreshed = condition_copy
        metadata.save()

        return HttpResponse(json.dumps(condition))
    else:
        raise Http404()


@login_required(login_url='/login')
def check_word_in_dictionary(request):
    if request.method == 'GET' and request.is_ajax():
        data = json.loads(request.GET.get('data', json.dumps({'word': ''})))
        word = data.get('word', '')
        """:type: unicode"""

        such_word_exists = dictionary.exists(word.encode(encoding='utf-8'))
        return HttpResponse(json.dumps(such_word_exists))


@login_required(login_url='/login')
def do_nothing(request, game_id):
    game = Game.objects.get(id=game_id)
    game.next_player()
    game.save()