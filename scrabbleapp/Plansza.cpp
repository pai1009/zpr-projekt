#include "Plansza.h"
#include <boost/python.hpp>

using namespace std;

Plansza::Plansza() {
    for (int i = 0; i < 15; ++i) {
        for (int j = 0; j < 15; ++j) {
            plansza[i][j] = new ZwyklePole(1);
        }
    }

    //Pola słowne ze współczynnikiem 3
    plansza[0][0] =  new PoleSlowne(3);
    plansza[0][7] =  new PoleSlowne(3);
    plansza[0][14] =  new PoleSlowne(3);
    plansza[0][7] =  new PoleSlowne(3);
    plansza[7][0] =  new PoleSlowne(3);
    plansza[14][0] =  new PoleSlowne(3);
    plansza[14][7] =  new PoleSlowne(3);
    plansza[14][14] =  new PoleSlowne(3);
    plansza[7][14] =  new PoleSlowne(3);

    //Pola slowne ze wspolczynnikiem 2
    plansza[1][1] =  new PoleSlowne(2);
    plansza[2][2] =  new PoleSlowne(2);
    plansza[3][3] =  new PoleSlowne(2);
    plansza[4][4] =  new PoleSlowne(2);
    plansza[1][13] =  new PoleSlowne(2);
    plansza[2][12] =  new PoleSlowne(2);
    plansza[3][11] =  new PoleSlowne(2);
    plansza[4][10] =  new PoleSlowne(2);
    plansza[13][1] =  new PoleSlowne(2);
    plansza[12][2] =  new PoleSlowne(2);
    plansza[11][3] =  new PoleSlowne(2);
    plansza[10][4] =  new PoleSlowne(2);
    plansza[13][13] =  new PoleSlowne(2);
    plansza[12][12] =  new PoleSlowne(2);
    plansza[11][11] =  new PoleSlowne(2);
    plansza[10][10] =  new PoleSlowne(2);
    plansza[7][7] =  new PoleSlowne(2);

    //Pola literowe ze wspolczynnikiem 3
    plansza[5][1] =  new PoleLiterowe(3);
    plansza[9][1] =  new PoleLiterowe(3);
    plansza[1][5] =  new PoleLiterowe(3);
    plansza[5][5] =  new PoleLiterowe(3);
    plansza[9][5] =  new PoleLiterowe(3);
    plansza[13][5] =  new PoleLiterowe(3);
    plansza[1][9] =  new PoleLiterowe(3);
    plansza[5][9] =  new PoleLiterowe(3);
    plansza[9][9] =  new PoleLiterowe(3);
    plansza[13][9] =  new PoleLiterowe(3);
    plansza[5][13] =  new PoleLiterowe(3);
    plansza[9][13] =  new PoleLiterowe(3);


    //Pola literowe ze wspolczynnikiem 2
    plansza[0][3] =  new PoleLiterowe(2);
    plansza[0][11] =  new PoleLiterowe(2);
    plansza[2][6] =  new PoleLiterowe(2);
    plansza[2][8] =  new PoleLiterowe(2);
    plansza[3][0] =  new PoleLiterowe(2);
    plansza[3][7] =  new PoleLiterowe(2);
    plansza[3][14] =  new PoleLiterowe(2);
    plansza[6][2] =  new PoleLiterowe(2);
    plansza[6][6] =  new PoleLiterowe(2);
    plansza[6][8] =  new PoleLiterowe(2);
    plansza[6][12] =  new PoleLiterowe(2);
    plansza[7][3] =  new PoleLiterowe(2);
    plansza[7][11] =  new PoleLiterowe(2);
    plansza[8][2] =  new PoleLiterowe(2);
    plansza[8][6] =  new PoleLiterowe(2);
    plansza[8][8] =  new PoleLiterowe(2);
    plansza[8][12] =  new PoleLiterowe(2);
    plansza[11][0] =  new PoleLiterowe(2);
    plansza[11][7] =  new PoleLiterowe(2);
    plansza[11][14] =  new PoleLiterowe(2);
    plansza[12][6] =  new PoleLiterowe(2);
    plansza[12][8] =  new PoleLiterowe(2);
    plansza[14][3] =  new PoleLiterowe(2);
    plansza[14][11] = new PoleLiterowe(2);
}

/**
Funkcja zwracajaca mnoznik punktowy za konkretne pole
*/
int Plansza::getPunktyZaPole(int x, int y) {
    return this->plansza[x][y]->getPunkty();
}

Plansza::~Plansza() {
    for (int i = 0; i < 15; i++) {
        for (int j = 0; j < 15; j++) {
            delete plansza[i][j];
        }
    }
}
