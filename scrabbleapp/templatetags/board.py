from django import template

register = template.Library()


@register.filter(name='times_from_one')
def times_from_one(number):
    return range(1, number + 1)


@register.simple_tag(name='cell_class')
def cell_class(row, column):
    if (row, column) == (8, 8):
        return "star"
    elif row in (1, 8, 15) and column in (1, 8, 15):
        return "tripleWord"
    elif row == column and (2 <= row <= 5 or 11 <= row <= 15) or row + column == 16 and (
                        2 <= row <= 5 or 11 <= row <= 15):
        return "doubleWord"
    elif (row, column) in (
            (1, 4), (1, 12), (3, 7), (3, 9), (4, 1), (4, 8), (4, 15), (7, 3), (7, 7), (7, 9), (7, 13), (8, 4), (8, 12),
            (9, 3), (9, 7), (9, 9), (9, 13), (12, 1), (12, 8), (12, 15), (13, 7), (13, 9), (15, 4), (15, 12)):
        return "doubleLetter"
    elif (row, column) in (
            (2, 6), (2, 10), (6, 2), (6, 6), (6, 10), (6, 14), (10, 2), (10, 6), (10, 10), (10, 14), (14, 6), (14, 10)):
        return "tripleLetter"
    else:
        return ""