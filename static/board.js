setInterval(function () {
    $.get('ajax/needs_reloading', {}, function (data) {
        var refresh = JSON.parse(data) === true;

        if (refresh) {
            location.reload();
        }
    })
}, 3000);

$(document).ready(function () {

    var droppedLetters = [];

    var dragOptions = {

        revert: "invalid",
        scope: "items",
        helper: "clone",
        cursor: "pointer",
        opacity: 0.50,

        stop: function () {

            var letterText = $(this).text().trim();

            var letterPosition = $(this).position();
            var boardPosition = $("#board-body").position();

            var letterLeftPosition = letterPosition.left - boardPosition.left - 3;
            var letterTopPosition = letterPosition.top - boardPosition.top - 3;


            droppedLetters.push({
                letter: $.trim(letterText),
                x: letterLeftPosition / 44,
                y: letterTopPosition / 44
            });

            $(this).removeClass('drag');
            $(this).css('font-weight', 'bold');
        }
    };
    $('.drag').draggable(dragOptions);

    $('#sendWord').click(function () {

        if (droppedLetters.length > 0) {
            var data_json = JSON.stringify(droppedLetters);

            $.get("ajax/add_word", {letters: data_json}, function (data) {
                if (JSON.parse(data) === false) {
                    alert('Dodane litery nie stanowią słowa. Spróbuj jeszcze raz.')
                }

                location.reload();
            });
        }

        droppedLetters = [];
    });

    $('#doNothing').click(function () {
        $.get('ajax/do_nothing');
        location.reload();
    });

    $("#board td").hover(
        function () {
            $(this).css("background", "yellow");
        },
        function () {
            $(this).css("background", "");
        }
    );

    $(".drag").hover(
        function () {
            $(this).css("background", "yellow");
        },
        function () {
            $(this).css("background", "");
        }
    );

    $(".player").hover(
        function () {
            $(this).css("background", "yellow");
        },
        function () {
            $(this).css("background", "");
        }
    );
});

$("#board td").droppable({

    scope: "items",
    hoverClass: "drop-hover",
    tolerance: "intersect",

    drop: function (e, ui) {

        var $drop = $(this);
        $(ui.draggable).draggable({
            "disabled": false
        }).appendTo($drop);
    }
});

$("#slownik").click(function () {
    var word = $('#text_field').val();

    $.get("/ajax/check_word", {data: JSON.stringify({word: word})}, function (data) {
        if (JSON.parse(data) === true) {
            alert('Słowo "' + word + '" istnieje w słowniku.')
        } else {
            alert('Słowo "' + word + '" nie istnieje w słowniku.')
        }
    });

    $('#text_field').val("");
});
