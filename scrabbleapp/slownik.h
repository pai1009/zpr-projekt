#include <string>
#include <vector>
#include <boost/python.hpp>

using namespace std;

class Dictionary {

	vector<string> dictionary;

public:
	Dictionary (const string& name);

	bool exists(string word);
};




using namespace boost::python;

BOOST_PYTHON_MODULE(dictionary)
{
    class_<Dictionary>("Dictionary", init<std::string>())
	.def("exists", &Dictionary::exists);
}
