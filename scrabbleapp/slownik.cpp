#include "slownik.h"
#include <algorithm>
#include <fstream>


Dictionary::Dictionary(const string& name) {

	dictionary.reserve(3000000);

	ifstream file(name.c_str());
	string word;

	while(file >> word) {

		dictionary.push_back(word);
	}
}
	
bool Dictionary::exists(string word){
	//return binary_search(dictionary.begin(), dictionary.end(), word);
	return find(dictionary.begin(), dictionary.end(), word) != dictionary.end();
}
