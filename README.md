==========
 Scrabble
==========

Gra wieloosobowa (scrabble)
zrealizowana jako "cienki klient" - przez przeglądarkę,


interfejs graficzny przy pomocy HTML + Java Script +jQuery, Python boost::python + C++


===============


Instrukcja do uruchomienia (linux):

Wymagane: django 1.6, python2, boost:python oraz paczki do Django: django-picklefield, django-enumfield 

$ cd scrabble


$ virtualenv --no-site-packages env


$ source env/bin/activate


$ pip install django


$ pip install django-picklefield


$ pip install django-enumfield


$ cd scrabbleapp


$ make


$ python manage.py syncdb


$ python run manage.py