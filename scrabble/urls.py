from django.conf.urls import patterns, include, url

from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'scrabble.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'scrabbleapp.views.main'),
    url(r'^ajax/check_word/$', 'scrabbleapp.views.check_word_in_dictionary'),
    url(r'^register/$', 'scrabbleapp.views.register'),
    url(r'^login/$', 'scrabbleapp.views.user_login'),
    url(r'^logout/$', 'scrabbleapp.views.user_logout'),
    url(r'^profile/$', 'scrabbleapp.views.profile'),
    url(r'^join/(?P<game_id>[0-9]+)/$', 'scrabbleapp.views.add_player_to_game'),
    url(r'^quit/(?P<game_id>[0-9]+)/$', 'scrabbleapp.views.remove_player_from_game'),
    url(r'^game/(?P<game_id>[0-9]+)/$', 'scrabbleapp.views.play_game'),
    url(r'^start/(?P<game_id>[0-9]+)/$', 'scrabbleapp.views.start_game'),
    url(r'^game/(?P<game_id>[0-9]+)/ajax/add_word/$', 'scrabbleapp.views.add_word'),
    url(r'^game/(?P<game_id>[0-9]+)/ajax/needs_reloading/$', 'scrabbleapp.views.needs_reloading'),
    url(r'^game/(?P<game_id>[0-9]+)/ajax/do_nothing/$', 'scrabbleapp.views.do_nothing'),
    url(r'^game/(?P<game_id>[0-9]+)/ajax/remove_player/$', 'scrabbleapp.views.remove_player_from_game'),
    url(r'^create/$', 'scrabbleapp.views.create_game'),
)
